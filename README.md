## 功能介绍 
    
随着国民健身意识越来越强，各式各样的健身模式不断出现。 瑜伽也受到了大众的喜爱，瑜伽行业发展越来越快,作为馆主，你还在微信群里让你的会员使用接龙的方式进行约课吗？你还在用传统的Excel进行排课吗？如果有一款小程序会员点一下就能约课，会不会让你惊喜、意外、激动——没错，瑜伽预约小程序就是为了解决馆主会员约课的痛点应运而生。功能包括瑜伽馆动态，瑜伽常识，瑜伽老师预约，瑜伽课程预约等模块。

- 瑜伽预约管理：开始/截止时间/人数均可灵活设置，可以自定义客户预约填写的数据项
- 瑜伽预约凭证：支持线下到场后校验签到/核销/二维码自助签到等多种方式
- 详尽的预约数据：支持预约名单数据导出Excel，打印
![输入图片说明](demo/%E4%BA%8C%E7%BB%B4%E7%A0%81.png)
 

## 技术运用
- 本项目使用微信小程序平台进行开发。
- 使用腾讯专门的小程序云开发技术，云资源包含云函数，数据库，带宽，存储空间，定时器等，资源配额价格低廉，无需域名和服务器即可搭建。
- 小程序本身的即用即走，适合小工具的使用场景，也适合快速开发迭代。
- 云开发技术采用腾讯内部链路，没有被黑客攻击的风险，安全性高且免维护。
- 资源承载力可根据业务发展需要随时弹性扩展。  



## 作者
- 如有疑问，欢迎骚扰联系我鸭：开发交流，技术分享，问题答疑，功能建议收集，版本更新通知，安装部署协助，小程序开发定制等。
- 俺的微信:

![输入图片说明](https://gitee.com/naive2021/smartcollege/raw/master/demo/author.jpg)



## 演示
![输入图片说明](demo/%E4%BA%8C%E7%BB%B4%E7%A0%81.png)
 

 

## 安装

- 安装手册见源码包里的word文档




## 截图

 ![输入图片说明](demo/%E9%A6%96%E9%A1%B5.png)
![输入图片说明](demo/%E5%B8%B8%E8%AF%86.png)

![输入图片说明](demo/%E5%8A%A8%E6%80%81.png)
![输入图片说明](demo/%E6%95%99%E7%BB%83%E9%A2%84%E7%BA%A6.png)
![输入图片说明](demo/%E8%AF%BE%E7%A8%8B%E9%A2%84%E7%BA%A6.png)

![输入图片说明](demo/%E6%97%A5%E5%8E%86.png)
![输入图片说明](demo/%E4%BA%BA.png)
![输入图片说明](demo/%E8%AF%BE%E7%A8%8B%E9%A2%84%E7%BA%A6.png)
![输入图片说明](demo/%E8%AF%A6%E6%83%85.png)

![输入图片说明](demo/%E9%A2%84%E7%BA%A6.png)
![输入图片说明](demo/%E9%A2%84%E7%BA%A6%E6%88%90%E5%8A%9F.png)
![输入图片说明](demo/%E6%88%91%E7%9A%84%E9%A2%84%E7%BA%A6.png)

## 后台管理系统截图


![输入图片说明](demo/%E5%90%8E%E5%8F%B0-%E9%A2%84%E7%BA%A6.png)
![输入图片说明](demo/%E5%90%8E%E5%8F%B0-%E9%A2%84%E7%BA%A6%E6%B7%BB%E5%8A%A0.png)
![输入图片说明](demo/%E5%90%8E%E5%8F%B0-%E6%97%B6%E6%AE%B5.png)
![输入图片说明](demo/%E5%90%8E%E5%8F%B0-%E5%90%8D%E5%8D%95.png)
![输入图片说明](demo/%E5%90%8E%E5%8F%B0-%E5%90%8D%E5%8D%95%E7%AE%A1%E7%90%86.png)

![输入图片说明](demo/%E5%90%8E%E5%8F%B0-%E6%A0%B8%E9%94%80.png)
![输入图片说明](demo/%E5%90%8E%E5%8F%B0-%E8%8F%9C%E5%8D%95.png)

![输入图片说明](demo/%E5%90%8E%E5%8F%B0-%E5%AF%BC%E5%87%BA.png)

